from time import time
import csv

FILENAME = "schools_data.csv"
data = []
stats = {"by_state": {}, "by_city": {}, "by_metro": {}}


def compute_stats(row):
    if row["LSTATE05"] not in stats["by_state"].keys():
        stats["by_state"][row["LSTATE05"]] = 1
    else:
        stats["by_state"][row["LSTATE05"]] += 1

    if row["LCITY05"] not in stats["by_city"].keys():
        stats["by_city"][row["LCITY05"]] = 1
    else:
        stats["by_city"][row["LCITY05"]] += 1

    if row["MLOCALE"] not in stats["by_metro"].keys():
        stats["by_metro"][row["MLOCALE"]] = 1
    else:
        stats["by_metro"][row["MLOCALE"]] += 1


def timeit(f):
    def wrapper(*args, **kwargs):
        before = time()
        rv = f(*args, **kwargs)
        after = time()
        print(f"Time elapsed {1000*(after - before):.2f}ms")
        return rv

    return wrapper


def total_schools():
    return len(data)


def state_stats():
    return stats["by_state"]


def city_stats():
    return stats["by_city"]


def metro_stats():
    return stats["by_metro"]


def city_with_most_schools():
    city = max(stats["by_city"], key=stats["by_city"].get)
    return (city, stats["by_city"][city])


def unique_cities_with_atleast_one_school():
    return len(list(filter(lambda stat: stat[1] > 0, stats["by_city"].items())))


@timeit
def print_counts(filename=FILENAME):
    with open(filename) as datafile:
        reader = csv.DictReader(datafile)
        for row in reader:
            data.append(row)
            compute_stats(row)

    print(f"Total Schools: {total_schools()}")

    print("Schools by State:")
    for state, count in stats["by_state"].items():
        print(f"{state} : {count}")

    print("Schools by Metro-centric Locale:")
    for metro, count in stats["by_metro"].items():
        print(f"{metro} : {count}")

    city, count = city_with_most_schools()
    print(f"City with most schools: {city} ({count} schools)")

    print(
        f"Unique cities with at least one school:{unique_cities_with_atleast_one_school()}"
    )
