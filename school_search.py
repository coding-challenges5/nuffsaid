from time import time

from utils import timeit, STATES_MAPPING, KeyNotFound

SCHOOLNAME_KEY = "SCHNAM05"
CITY_KEY = "LCITY05"
STATECODE_KEY = "LSTATE05"
si = SearchIndex()


class SearchIndex:
    CORPUS_KEY = "corpus"

    def __init__(self, *args, **kwargs):
        # Index Data is stored as blocks of indexes depending on their word length
        # eg., {'5': {'middle': [...], 'school': [...]}}
        self._indexdata = {}
        self.keyspace = []
        super(SearchIndex, self).__init__(*args, **kwargs)

    def update(self, key, document):
        """
        Update index with key and document associated.
        """
        block_size = len(key)
        if block_size not in self._indexdata.keys():
            self._indexdata[block_size] = {}
            self._indexdata[block_size].update({key: [document]})
            self.keyspace.append(key)
        else:
            if key not in self._indexdata[block_size].keys():
                self._indexdata[block_size].update({key: [document]})
                self.keyspace.append(key)
            else:
                self._indexdata[block_size][key].append(document)

    def get(self, key):
        """
        Get documents indexed for a key
        """
        if key not in self.keyspace:
            raise KeyNotFound()
        block_size = len(key)
        return self._indexdata[block_size][key]

    @staticmethod
    def match_score(words, phrase):
        """
        Matching score between a list of words and a phrase.
        """
        matching_words = list(filter(lambda w: w in words, phrase.split()))
        return len(set(matching_words))

    @staticmethod
    def top_results(results, limit):
        """
        Top results rated on matching score.
        """
        results.sort(reverse=True, key=lambda result: result[1])
        if len(results) < limit:
            return [result[0] for result in results]
        else:
            return [result[0] for result in results[:limit]]

    def search(self, phrase, limit=3):
        """
        Search a phrase to extract top results
        """
        words = phrase.split(" ")
        words = [word.lstrip().rstrip().upper() for word in words]
        results = []
        processed = []
        matching_scores = {}
        for word in words:
            try:
                matches = self.get(word)
                for match in matches:
                    if match[self.CORPUS_KEY] not in matching_scores.keys():
                        matching_scores[match[self.CORPUS_KEY]] = self.match_score(
                            words, match[self.CORPUS_KEY]
                        )
                        results.append([match, matching_scores[match[self.CORPUS_KEY]]])

            except KeyNotFound as ex:
                continue

        return self.top_results(results, limit)

    @timeit
    def build_index(self, data, keys):
        """
        Build index data.
        """
        records = 0
        for row in data:
            words = []
            document = {}
            for key in keys:
                words.extend(row[key].split())
                document[key] = row[key]
            phrase = " ".join(words)
            document[self.CORPUS_KEY] = phrase
            for word in set(words):
                self.update(word, document)
                records += 1
        print(f"{records} Records built.")


def build_schools_index(search_index):
    """
    Populate schools index data
    """
    import csv

    filename = "schools_data.csv"
    data = []
    with open(filename) as datafile:
        reader = csv.DictReader(datafile)
        for index, row in enumerate(reader):
            row["state"] = STATES_MAPPING[row[STATECODE_KEY]]
            row["index"] = index
            data.append(row)
        search_index.build_index(
            data=data, keys=[CITY_KEY, SCHOOLNAME_KEY, STATECODE_KEY, "state"]
        )


build_schools_index(search_index=si)


def search_schools(query, count=3):
    """
    Search schools by string
    """
    before = time()
    schools = si.search(query, limit=count)
    if not schools:
        print(f"No Results Found (search took: {time() - before:.3f}s)")
    else:
        print(f'Results for "{query}" (search took: {time() - before:.3f}s)')
        for index, school in enumerate(schools, start=1):
            print(f"{index}.{school[SCHOOLNAME_KEY]}")
            print(f"{school[CITY_KEY]}, {school[STATECODE_KEY]}")
