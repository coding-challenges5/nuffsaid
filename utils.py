from time import time


def timeit(f):
    def wrapper(*args, **kwargs):
        before = time()
        rv = f(*args, **kwargs)
        after = time()
        print(f"Time elapsed {(after - before):.3f}s")
        return rv

    return wrapper


class KeyNotFound(Exception):
    def __init__(self, *args, **kwargs):
        super(KeyNotFound, self).__init__(*args, **kwargs)

    def repr(self):
        return "Key not found"


STATES_MAPPING = {
    "AZ": "ARIZONA",
    "HI": "HAWAII",
    "DE": "DELAWARE",
    "AR": "ARKANSAS",
    "FL": "FLORIDA",
    "AK": "ALASKA",
    "AL": "ALABAMA",
    "MD": "MARYLAND",
    "OH": "OHIO",
    "OR": "OREGON",
    "IN": "INDIANA",
    "ID": "IDAHO",
    "CA": "CALIFORNIA",
    "CO": "COLARADO",
    "GA": "GEORGIA",
    "CT": "CONNECTICUT",
    "IL": "ILLINOIS",
    "NV": "NEVADA",
    "C": "",
    "DC": "WASHINGTON DC",
    "IA": "IOWA",
}
